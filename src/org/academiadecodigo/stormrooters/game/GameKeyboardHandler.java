package org.academiadecodigo.stormrooters.game;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class GameKeyboardHandler implements KeyboardHandler {

    public void chooseBinding(){
        Keyboard k = new Keyboard(this);

        int[] keys = {
                KeyboardEvent.KEY_1,
                KeyboardEvent.KEY_2,
                KeyboardEvent.KEY_3,
        };

        for (int key : keys) {
            KeyboardEvent event = new KeyboardEvent();
            event.setKey(key);
            event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            k.addEventListener(event);
        }

    }

    public void controls(KeyBindings keyBinding) {
        Keyboard k = new Keyboard(this);

        int[] keys = new int[5];

        switch (keyBinding){
            case VIM:
                keys = new int[]{
                        KeyboardEvent.KEY_H,
                        KeyboardEvent.KEY_J,
                        KeyboardEvent.KEY_K,
                        KeyboardEvent.KEY_L,
                        KeyboardEvent.KEY_SPACE
                };
                break;

            case WASD:
                keys = new int[]{KeyboardEvent.KEY_W,
                        KeyboardEvent.KEY_A,
                        KeyboardEvent.KEY_S,
                        KeyboardEvent.KEY_D,
                        KeyboardEvent.KEY_SPACE
                };
                break;

            case ARROWS:
                keys = new int[]{
                        KeyboardEvent.KEY_DOWN,
                        KeyboardEvent.KEY_LEFT,
                        KeyboardEvent.KEY_RIGHT,
                        KeyboardEvent.KEY_UP,
                        KeyboardEvent.KEY_SPACE
                };
                break;
        }

        for (int key : keys) {
            KeyboardEvent event = new KeyboardEvent();
            event.setKey(key);
            event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            k.addEventListener(event);
        }

    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
