package org.academiadecodigo.stormrooters.game;

public enum KeyBindings {

    VIM,
    WASD,
    ARROWS;
}
