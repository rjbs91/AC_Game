package org.academiadecodigo.stormrooters.game;

import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stormrooters.game.gameObjects.*;
import org.academiadecodigo.stormrooters.game.mapStuff.CollisionDetector;
import org.academiadecodigo.stormrooters.game.mapStuff.Field;
import org.academiadecodigo.stormrooters.game.mapStuff.Map;
import java.util.LinkedList;
import java.util.List;

public class Game extends GameKeyboardHandler {

    private static final String LEVEL_COUNTER = "Level: %s";

    private Player player;
    private Field field;
    private Map map;
    private int delay;
    private int counter;
    private Text level;


    private LinkedList<Trap> traps = new LinkedList<>();
    private LinkedList<Lollipop> lollipops = new LinkedList<>();
    private LinkedList<Door> doors = new LinkedList<>();

    private int stepsToTrigger;
    private int stepsTaken = 0;
    private int lollipopsCollected = 0;
    private int startingLollipops;
    private boolean keyPressed;
    private KeyboardEvent key;
    private int difficulty = 0;

    private Sound lick = new Sound("/assets/lick.wav");
    private Sound theme = new Sound("/assets/mainTheme.wav");
    private Sound gameOver = new Sound("/assets/gameover.wav");

    public Game(int cols, int rows) {

        field = new Field(cols, rows);

        this.delay = 100;
    }


    public void init() throws InterruptedException {

        Picture titleScreen = makePicture("assets/LogoMAXpowerbackground.jpg");

        Thread.sleep(3500);

        titleScreen.delete();

        gameStartScreen();

    }

    private void start() throws InterruptedException {

        counter = 0;
        while (true) {

            counter++;

            Thread.sleep(delay);

            if (keyPressed && key.getKey() != KeyboardEvent.KEY_SPACE) {
                movePLayer();
            }

            if (!traps.isEmpty()) {

                flashTrap();
            }

            checkCollisions(traps);
            checkCollisions(lollipops);

            if (lollipopsCollected == startingLollipops) {

                revealExit();
            }
        }
    }

    private void revealExit() throws InterruptedException {

        doors.add(new Door(field));
        doors.getFirst().makeVisible(true);
        checkCollisions(doors);
    }

    private void flashTrap() throws InterruptedException {
        if (counter == 2 || counter == 4) {
            traps.getLast().getPos().show();
            Thread.sleep(50);
            traps.getLast().getPos().hide();
        }

        if (counter == 6) {
            traps.getLast().getPos().show();
        }

    }

    private void movePLayer() {
        keyPressed = false;
        player.move(key);
        stepsTaken++;

        if (stepsTaken % stepsToTrigger == 0) {
            buildTrap();
            counter = 0;

            for (Trap trap : traps) {
                trap.getPos().show();
            }
        }
    }

    private void checkCollisions(LinkedList linkedList) throws InterruptedException {

        if (linkedList.peek() instanceof Door) {

            CollisionDetector<Door> collisionDetector = new CollisionDetector<>(doors);

            if (collisionDetector.hasCollided(player)) {

                difficulty++;
                fieldClear();
                newLevel();
                return;
            }
        }

        if (linkedList.peek() instanceof Trap) {

            CollisionDetector<Trap> collisionDetector = new CollisionDetector<>(traps);

            if (collisionDetector.hasCollided(player)) {

                theme.stop();
                gameOver.play(true);

                map.clearMap();

                difficulty = 0;
                fieldClear();

                Picture wastedScreen = makePicture("assets/DeathScreen.jpg");

                while (true) {

                    Thread.sleep(1);

                    if (keyPressed) {

                        keyPressed = false;
                        if (key.getKey() == KeyboardEvent.KEY_SPACE) {

                            wastedScreen.delete();
                            break;
                        }
                    }
                }


                gameStartScreen();
                return;
            }
        }

        if (linkedList.peek() instanceof Lollipop) {

            CollisionDetector<Lollipop> collisionDetector = new CollisionDetector<>(lollipops);

            if (collisionDetector.hasCollided(player)) {
                lollipopsCollected++;
                lick.play(true);
            }
        }
    }

    private void newLevel() throws InterruptedException {

        level = new Text(Field.PADDING, 0, String.format(LEVEL_COUNTER, difficulty));
        level.translate( (field.getWidth() / 2) - (level.getWidth() / 2), 0);
        level.draw();

        if (difficulty == 0) {

            stepsToTrigger = 5;
            startingLollipops = 10;
        }

        if (difficulty % 2 == 0) {

            stepsToTrigger--;

            if (stepsToTrigger <= 0) {

                stepsToTrigger = 1;
            }

            startingLollipops++;
        }

        lollipopsCollected = 0;

        for (int i = 0; i < startingLollipops; i++) {

            Lollipop newLollipop = new Lollipop(field);
            boolean safe = false;

            while (!safe) {

                safe = true;

                for (Lollipop lollipop : lollipops) {

                    if (newLollipop.getPos().equals(lollipop.getPos())) {

                        newLollipop = new Lollipop(field);
                        safe = false;
                    }
                }

                if (newLollipop.getPos().getCol() == field.getCols() - 1 &&
                        newLollipop.getPos().getRow() == field.getRows() - 1 ||
                        newLollipop.getPos().getCol() == 0 &&
                                newLollipop.getPos().getRow() == 0) {

                    newLollipop = new Lollipop(field);
                    safe = false;
                }

            }
            newLollipop.makeVisible(true);
            lollipops.add(newLollipop);
        }

        field.init();
        player = new Player(field);
        start();
    }

    private void fieldClear() {

        List<GameObject> objects = new LinkedList<>(traps);
        objects.addAll(lollipops);
        objects.addAll(doors);
        objects.add(player);

        for (GameObject gameObject : objects){
            gameObject.makeVisible(false);
        }

        traps.clear();
        lollipops.clear();
        doors.clear();
        level.delete();

        stepsTaken = 0;

    }

    private void buildTrap() {

        Trap trap = new Trap(field);
        boolean safe = false;

        List<GameObject> objects = new LinkedList<>(lollipops);
        objects.addAll(traps);

        if (doors != null && !doors.isEmpty()) {
            objects.addAll(doors);
        }

        objects.add(player);

        while (!safe) {

            safe = true;

            for (GameObject gameObject: objects) {

                if (trap.getPos().equals(gameObject.getPos())) {
                    trap = new Trap(field);
                    safe = false;
                }

                if (trap.getPos().getCol() == field.getCols() -1 &&
                        trap.getPos().getRow() == field.getRows() -1){
                    trap = new Trap(field);
                    safe = false;
                }

            }
        }

        traps.add(trap);
    }

    private void gameStartScreen() throws InterruptedException {

        Picture gameLogo = makePicture("assets/menuKeybinding.jpg");

        theme.loopIndef();
        theme.play(true);

        chooseBinding();

        while (true) {

            Thread.sleep(1);

            if (keyPressed && key.getKey() != KeyboardEvent.KEY_SPACE) {

                switch (key.getKey()) {

                    case KeyboardEvent.KEY_1:
                        controls(KeyBindings.VIM);
                        break;

                    case KeyboardEvent.KEY_2:
                        controls(KeyBindings.WASD);
                        break;

                    case KeyboardEvent.KEY_3:
                        controls(KeyBindings.ARROWS);
                        break;
                }

                keyPressed = false;
                gameLogo.delete();
                break;
            }
        }

        Picture gameLogo2 = makePicture("assets/menu2FINAL.jpg");

        while (true) {

            Thread.sleep(100);

            if (keyPressed) {

                if (key.getKey() == KeyboardEvent.KEY_SPACE) {

                    gameLogo2.delete();
                    keyPressed = false;

                    map = new Map(field);
                    newLevel();

                }
            }

        }
    }

    private Picture makePicture(String path) {

        Picture picture = new Picture(field.columnToX(0), field.rowToY(0), path);
        picture.grow(-(picture.getWidth() - field.getWidth()) / 2, -(picture.getHeight() - field.getHeight()) / 2);
        picture.translate(field.columnToX(0) - picture.getX(), field.rowToY(0) - picture.getY());
        picture.draw();

        return picture;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {


        keyPressed = true;
        key = keyboardEvent;

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }


}



