package org.academiadecodigo.stormrooters.game.grid.position;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stormrooters.game.gameObjects.Player;
import org.academiadecodigo.stormrooters.game.grid.GridDirection;
import org.academiadecodigo.stormrooters.game.mapStuff.Field;

public class Position {

    private int col;
    private int row;
    private Field grid;
    private Picture pic;

    public Position(Field grid, int col, int row, String asset) {
        this.col = col;
        this.row = row;
        this.grid = grid; // give the grid to the position

        pic = new Picture(grid.columnToX(col), grid.rowToY(row), asset);
        pic.grow(-(pic.getWidth() - grid.getCellSize()) / 2, -(pic.getHeight() - grid.getCellSize()) / 2);
        pic.translate(grid.columnToX(col) - pic.getX(), grid.rowToY(row) - pic.getY());

        if (asset.equals(Player.ASSET_DOWN)) {
            pic.draw();
        }

    }

    public void moveInDirection(GridDirection direction) {

        switch (direction) {

            case UP:
                moveUp();
                pic.delete();
                pic.load(Player.ASSET_UP);
                pic.draw();
                break;

            case DOWN:
                moveDown();
                pic.delete();
                pic.load(Player.ASSET_DOWN);
                pic.draw();
                break;

            case LEFT:
                moveLeft();
                pic.delete();
                pic.load(Player.ASSET_LEFT);
                pic.draw();
                break;

            case RIGHT:
                moveRight();
                pic.delete();
                pic.load(Player.ASSET_RIGHT);
                pic.draw();
                break;
        }
        pic.translate(grid.columnToX(col) - pic.getX(),
                grid.rowToY(row) - pic.getY());

    }

    public boolean equals(Position pos) {
        return this.col == pos.getCol() && this.row == pos.getRow();
    }

    private void moveUp() {

        if (row <= 0){
            row = 0;
            return;
        }
        row--;

    }

    private void moveDown() {

        if (row >= grid.getRows() -1){
            row = grid.getRows() -1;
            return;
        }
        row++;

    }

    private void moveLeft() {

        if (col <= 0){
            col = 0;
            return;
        }
        col--;

    }

    private void moveRight() {

        if (col >= grid.getCols() - 1){
            col = grid.getCols() -1;
            return;
        }
        col++;

    }

    public void show(){
        pic.draw();
    }

    public void hide(){
        pic.delete();
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }


    @Override
    public String toString() {
        return "Position{" +
                "col=" + col +
                ", row=" + row +
                '}';
    }
}
