package org.academiadecodigo.stormrooters.game.grid;


/**
 * The directions in which positions may move
 */
public enum GridDirection {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NONE;

}
