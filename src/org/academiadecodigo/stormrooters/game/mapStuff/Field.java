package org.academiadecodigo.stormrooters.game.mapStuff;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.stormrooters.game.grid.position.Position;

public class Field {

    public static final int PADDING = 10;
    private int cols;
    private int rows;
    private Rectangle field;
    private int cellSize = 50;

    public Field(int cols, int rows){
        this.cols = cols;
        this.rows = rows;
    }

    public void init(){

        this.field = new Rectangle(PADDING, PADDING * 2, getWidth(), getHeight());

    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }

    public int getWidth(){
        return cellSize * cols;
    }

    public int getHeight(){
        return cellSize * rows;
    }

    public int getCellSize() {
        return cellSize;
    }

    public Position makeFieldPosition(int col, int row, String asset){
        return new Position(this, col, row, asset);
    }

    public int rowToY(int row){
        return row * cellSize + (PADDING * 2);
    }

    public int columnToX(int col){
        return col * cellSize + PADDING;
    }
}
