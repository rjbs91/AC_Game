package org.academiadecodigo.stormrooters.game.mapStuff;

import org.academiadecodigo.stormrooters.game.gameObjects.Player;
import org.academiadecodigo.stormrooters.game.gameObjects.*;

import java.util.LinkedList;

public class CollisionDetector<T extends GameObject> {


    private LinkedList<T> gameObjects;

    public CollisionDetector(LinkedList<T> gameObjects) {
        this.gameObjects = gameObjects;
    }


    public boolean hasCollided(Player player) {
        for (T object : gameObjects) {

            if (object.getType().equals(ObjectType.DOOR) && player.getPos().equals(object.getPos()) &&
                    object.isVisible()) {

                object.makeVisible(false);
                player.makeVisible(false);

                return true;
            }

            if (object.getType().equals(ObjectType.LOLLIPOP) && player.getPos().equals(object.getPos()) &&
                    object.isVisible()) {

                object.makeVisible(false);
                return true;
            }

            if (object.getType().equals(ObjectType.TRAP) && player.getPos().equals(object.getPos())) {

                player.setAlive(false);
                return true;
            }
        }

        return false;
    }


}


