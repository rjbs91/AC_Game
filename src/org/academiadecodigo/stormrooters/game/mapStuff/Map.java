package org.academiadecodigo.stormrooters.game.mapStuff;


import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import java.util.LinkedList;

public class Map {

    private LinkedList<Picture> map = new LinkedList<>();

    public Map(Field field){


        for (int col = 0; col < field.getCols(); col++) {
            for (int row = 0; row < field.getRows(); row++) {

                Picture pic = new Picture(field.columnToX(col), field.rowToY(row), "assets/floorBLUE.jpeg");
                pic.grow(-(pic.getWidth() - field.getCellSize()) / 2, -(pic.getHeight() - field.getCellSize()) / 2);
                pic.translate(field.columnToX(col) - pic.getX(), field.rowToY(row) - pic.getY());
                pic.draw();

                map.add(pic);
            }
        }
    }

    public void clearMap(){
        for (Picture picture : map) {
            picture.delete();
        }
        map.clear();
    }
}
