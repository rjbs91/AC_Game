package org.academiadecodigo.stormrooters.game.gameObjects;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.stormrooters.game.mapStuff.Field;
import org.academiadecodigo.stormrooters.game.grid.GridDirection;

public class Player extends GameObject {

    public static final String ASSET_DOWN = "assets/DOWN.png";
    public static final String ASSET_UP = "assets/UP.png";
    public static final String ASSET_LEFT = "assets/LEFT.png";
    public static final String ASSET_RIGHT = "assets/RIGHT.png";
    private boolean alive = true;

    public Player(Field field) {

        super(field.makeFieldPosition(0, 0, ASSET_DOWN), ObjectType.PLAYER);

    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void move(KeyboardEvent keyboardEvent) {

        if (!isAlive()) {
            return;
        }
        GridDirection direction = getDirection(keyboardEvent);
        getPos().moveInDirection(direction);
    }

    public GridDirection getDirection(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_D:
            case KeyboardEvent.KEY_RIGHT:
            case KeyboardEvent.KEY_L:
                return GridDirection.RIGHT;

            case KeyboardEvent.KEY_A:
            case KeyboardEvent.KEY_LEFT:
            case KeyboardEvent.KEY_H:
                return GridDirection.LEFT;

            case KeyboardEvent.KEY_S:
            case KeyboardEvent.KEY_DOWN:
            case KeyboardEvent.KEY_J:
                return GridDirection.DOWN;

            case KeyboardEvent.KEY_W:
            case KeyboardEvent.KEY_UP:
            case KeyboardEvent.KEY_K:
                return GridDirection.UP;
        }
        return GridDirection.NONE;
    }

}
