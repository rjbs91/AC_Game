package org.academiadecodigo.stormrooters.game.gameObjects;

import org.academiadecodigo.stormrooters.game.grid.position.Position;

public abstract class GameObject {

    private boolean isVisible;
    private Position pos;
    private ObjectType type;

    public GameObject(Position pos, ObjectType type) {
        this.pos = pos;
        this.type = type;
        switch (type) {
            case LOLLIPOP:
                isVisible = true;
                break;
            case TRAP:
                isVisible = false;
                break;
            case DOOR:
                isVisible = false;
                break;
            case PLAYER:
                isVisible = true;
                break;
        }

    }

    public Position getPos() {
        return pos;
    }

    public void setPos(Position pos) {
        this.pos = pos;
    }

    public void makeVisible(boolean visible) {

        this.isVisible = visible;
        if (!visible) {

            getPos().hide();
            return;
        }
        getPos().show();

    }

    public boolean isVisible() {
        return isVisible;
    }

    public ObjectType getType() {
        return type;
    }
}
