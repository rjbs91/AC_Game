package org.academiadecodigo.stormrooters.game.gameObjects;

import org.academiadecodigo.stormrooters.game.mapStuff.Field;

public class Door extends GameObject {

    private static final String ASSET = "assets/stairs.png";

    public Door(Field field) {
        super(field.makeFieldPosition(field.getCols() - 1, field.getRows() - 1,
                ASSET), ObjectType.DOOR);
    }


    @Override
    public void makeVisible(boolean visible) {
        super.makeVisible(visible);
    }
}
