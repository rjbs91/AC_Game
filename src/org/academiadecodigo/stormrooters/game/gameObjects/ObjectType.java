package org.academiadecodigo.stormrooters.game.gameObjects;

public enum ObjectType {
    TRAP,
    LOLLIPOP,
    DOOR,
    PLAYER

}
