package org.academiadecodigo.stormrooters.game.gameObjects;

import org.academiadecodigo.stormrooters.game.mapStuff.Field;

public class Lollipop extends GameObject {

    private static final String ASSET = "assets/LOLLIE.png";

    public Lollipop(Field field) {
        super(field.makeFieldPosition((int) (Math.random() * field.getCols()), (int) (Math.random() * field.getRows()),
                ASSET), ObjectType.LOLLIPOP);

    }



}


