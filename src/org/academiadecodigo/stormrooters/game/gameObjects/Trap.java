package org.academiadecodigo.stormrooters.game.gameObjects;

import org.academiadecodigo.stormrooters.game.mapStuff.Field;

public class Trap extends GameObject {

    private static final String TRAP = "assets/trap3.png";

    public Trap(Field field) {
        super(field.makeFieldPosition((int) (Math.random() * field.getCols()), (int) (Math.random() * field.getRows()),
                TRAP), ObjectType.TRAP);
    }

    @Override
    public void makeVisible(boolean visible) {
        super.makeVisible(visible);
    }
}
